package com.ooptask.passive.view;

@FunctionalInterface
public interface Printable {

  void print();
}
