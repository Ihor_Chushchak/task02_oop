package com.ooptask.passive.view;

import com.ooptask.passive.controller.*;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  public static Scanner input = new Scanner(System.in);

  public View() {
    controller = new ControllerImpl();
    menu = new LinkedHashMap<String, String>();
    menu.put("1", "  1 - print plumbing");
    menu.put("2", "  2 - print tree stuff");
    menu.put("3", "  3 - print decoration");
    menu.put("Q", "  Q - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);


  }
  private void pressButton1() {
    System.out.println(controller.outputMenu1());
  }

  private void pressButton2() {
    System.out.println(controller.outputMenu2());
  }

  private void pressButton3() {
    System.out.println(controller.outputMenu3());
  }
 //----------------------------
  public static void output(Map<String, String> menu) {
    System.out.println("\nMENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  private void outputMenu() {
      System.out.println("\nMENU:");
      for (String str : menu.values()) {
        System.out.println(str);
      }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }


}
