package com.ooptask.passive.model;

import com.ooptask.passive.view.View;
import com.ooptask.passive.view.Printable;

import java.util.*;
import java.util.List;

public class Method extends Data {

  private Map<String, Printable> methodsMenu1;
  private Map<String, Printable> methodsMenu2;
  private Map<String, Printable> methodsMenu3;

  List<Integer> outputMenu1() {
   View.output(plumbing);

    methodsMenu1 = new LinkedHashMap<>();
    methodsMenu1.put("1", this::pressButtonWashbasin);
    methodsMenu1.put("2", this::pressButtonToilet);
    methodsMenu1.put("4", this::pressFindWashbasin);
    methodsMenu1.put("5", this::pressFindToilet);
    String keyMenu;
    do {
      System.out.println("Please, select menu point.");
      keyMenu = View.input.nextLine().toUpperCase();
      try {
        methodsMenu1.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
    return null;
  }

  private void pressButtonWashbasin() {
    View.output(washbasin);
    }

  private void pressFindWashbasin() {
    washbasin.put("1", "254");
    washbasin.put("2", "192");
    washbasin.put("3", "147");
    System.out.print("Please input price amount:");
    int amount = Integer.parseInt(View.input.nextLine());
    for (String str : washbasin.values()) {
      if (Integer.parseInt(String.valueOf(str)) <= amount) {
        System.out.println("Available Washbasin for " + str + "$");
      }
    }
    washbasin.put("1", "  Wooden washbasin for 254$");
    washbasin.put("2", "  Glass washbasin for 192$");
    washbasin.put("3", "  Ceramic washbasin for 147$");
  }

  private void pressButtonToilet() {
    View.output(toilet);
  }

  private void pressFindToilet() {
    toilet.put("1", "1959");
    toilet.put("2", "98");
    toilet.put("3", "129");
    System.out.print("Please input price amount:");
    int amount = Integer.parseInt(View.input.nextLine());
    for (String str : toilet.values()) {
      if (Integer.parseInt(String.valueOf(str)) <= amount) {
        System.out.println("Available Toilet bowl for " + str + "$");
      }
    }
    toilet.put("1", "  Golden toilet bowl for 1959$");
    toilet.put("2", "  White ceramic toilet bowl for 98$");
    toilet.put("3", "  Black ceramic toilet bowl for 129$");
  }
  List<Integer> outputMenu2() {
    View.output(tree);

    methodsMenu2 = new LinkedHashMap<>();
    methodsMenu2.put("1", this::pressButtonDoor);
    methodsMenu2.put("2", this::pressButtonFloor);
    methodsMenu2.put("4", this::pressFindDoor);
    methodsMenu2.put("5", this::pressFindFloor);
    String keyMenu;
    do {
      System.out.println("Please, select menu point.");
      keyMenu = View.input.nextLine().toUpperCase();
      try {
        methodsMenu2.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
    return null;
  }
  private void pressButtonDoor() {
    View.output(door);
  }

  private void pressFindDoor() {
    door.put("1", "130");
    door.put("2", "75");
    door.put("3", "30");
    System.out.print("Please input price amount:");
    int amount = Integer.parseInt(View.input.nextLine());
    for (String str : door.values()) {
      if (Integer.parseInt(String.valueOf(str)) <= amount) {
        System.out.println("Available door for " + str + "$");
      }
    }
    door.put("1", "  Fully wooden door for 130$");
    door.put("2", "  Half wooden door for 75$");
    door.put("3", "  Non wooden door for 30$");
  }
  private void pressButtonFloor() {
    View.output(floor);
  }

  private void pressFindFloor() {
    floor.put("1", "255");
    floor.put("2", "300");
    floor.put("3", "456");
    System.out.print("Please input price amount:");
    int amount = Integer.parseInt(View.input.nextLine());
    for (String str : floor.values()) {
      if (Integer.parseInt(String.valueOf(str)) <= amount) {
        System.out.println("Available floor for " + str + "$");
      }
    }
    floor.put("1", "  Laminate for 255$ for m^2");
    floor.put("2", "  Oak floor for 300$ for m^2");
    floor.put("3", "  Red wood floor for 456$ for m^2");
  }
  List<Integer> outputMenu3() {
    View.output(wall);

    methodsMenu3 = new LinkedHashMap<>();
    methodsMenu3.put("1", this::pressButtonPaint);
    methodsMenu3.put("4", this::pressFindPaint);
    String keyMenu;
    do {
      System.out.println("Please, select menu point.");
      keyMenu = View.input.nextLine().toUpperCase();
      try {
        methodsMenu3.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
    return null;
  }
  private void pressButtonPaint() {
    View.output(paint);
  }

  private void pressFindPaint() {
    paint.put("1", "29");
    paint.put("2", "39");
    paint.put("3", "45");
    System.out.print("Please input price amount:");
    int amount = Integer.parseInt(View.input.nextLine());
    for (String str : paint.values()) {
      if (Integer.parseInt(String.valueOf(str)) <= amount) {
        System.out.println("Available paint for " + str + "$");
      }
    }
    paint.put("1", "  Black for 29$ per 1L");
    paint.put("2", "  White  for 30$ per 1L");
    paint.put("3", "  Own washbasin for 45$ per 1L");
  }
}
