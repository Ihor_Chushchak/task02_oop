package com.ooptask.passive.model;

import java.util.LinkedHashMap;
import java.util.Map;

public class Data{
    public Map<String, String> plumbing;
    public Map<String, String> washbasin;
    public Map<String, String> toilet;
    public Map<String, String> tree;
    public Map<String, String> door;
    public Map<String, String> floor;
    public Map<String, String> wall;
    public Map<String, String> paint;
    public Data(){
    plumbing = new LinkedHashMap<String, String>();
    plumbing.put("1", "  1 - show washbasin");
    plumbing.put("2", "  2 - show toilet bowl");
    plumbing.put("Q", "  Q - exit");

    washbasin = new LinkedHashMap<String, String>();
    washbasin.put("1", "  Wooden washbasin for 254$");
    washbasin.put("2", "  Glass washbasin for 192$");
    washbasin.put("3", "  Ceramic washbasin for 147$");
    washbasin.put("4", "  4 - Find for price");
    washbasin.put("Q", "  Q - exit");

    toilet = new LinkedHashMap<String, String>();
    toilet.put("1", "  Golden toilet bowl for 1959$");
    toilet.put("2", "  White ceramic toilet bowl for 98$");
    toilet.put("3", "  Black ceramic toilet bowl for 129$");
    toilet.put("5", "  5 - Find for price");
    toilet.put("Q", "  Q - exit");

        tree = new LinkedHashMap<String, String>();
        tree.put("1", "  1 - show door");
        tree.put("2", "  2 - show floor");
        tree.put("Q", "  Q - exit");

        door = new LinkedHashMap<String, String>();
        door.put("1", "  Fully wooden door for 130$");
        door.put("2", "  Half wooden door for 75$");
        door.put("3", "  Non wooden door for 30$");
        door.put("4", "  4 - Find for price");
        door.put("Q", "  Q - exit");

        floor = new LinkedHashMap<String, String>();
        floor.put("1", "  Laminate for 255$ for m^2");
        floor.put("2", "  Oak floor for 300$ for m^2");
        floor.put("3", "  Red wood floor for 456$ for m^2");
        floor.put("5", "  5 - Find for price");
        floor.put("Q", "  Q - exit");

        wall = new LinkedHashMap<String, String>();
        wall.put("1", "  1 - show paint");
        wall.put("Q", "  Q - exit");

        paint = new LinkedHashMap<String, String>();
        paint.put("1", "  Black for 29$ for 1L");
        paint.put("2", "  White  for 30$ for 1L");
        paint.put("3", "  Own washbasin for 45$ for 1L");
        paint.put("4", "  4 - Find for price");
        paint.put("Q", "  Q - exit");
    }
}
