package com.ooptask.passive.model;

import java.util.List;

public class Logic implements Model {

  private Method method;

  public Logic() {
    method = new Method();
  }

  @Override
  public List<Integer> outputMenu1() {
    return method.outputMenu1();
  }
  @Override
  public List<Integer> outputMenu2() {
    return method.outputMenu2();
  }
  @Override
  public List<Integer> outputMenu3() {
    return method.outputMenu3();
  }

}
