package com.ooptask.passive.model;

import java.util.List;

public interface Model {

  List<Integer> outputMenu1();
  List<Integer> outputMenu2();
  List<Integer> outputMenu3();
}
