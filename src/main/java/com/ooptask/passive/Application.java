package com.ooptask.passive;

import com.ooptask.passive.view.*;

public class Application {

  public static void main(String[] args) {
    new View().show();
  }
}
