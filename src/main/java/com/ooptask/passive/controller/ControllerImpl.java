package com.ooptask.passive.controller;

import com.ooptask.passive.model.*;
import java.util.List;

public class ControllerImpl implements Controller {
  private Model model;
  public ControllerImpl() {
    model = new Logic();
  }
  @Override
  public List<Integer> outputMenu1() {
    return model.outputMenu1();
  }
  @Override
  public List<Integer> outputMenu2() {
    return model.outputMenu2();
  }
  @Override
  public List<Integer> outputMenu3() {
    return model.outputMenu3();
  }
}
